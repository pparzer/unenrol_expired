# Unenroll expired enrolments #

Unenroll expired participants from all moodle courses. 
If the enrolment expire date is defined and is less then now,
than the participant is unenrolled from the course.
This is helpful to clear inactive participants from the list of course participants.

To manually unenroll expired participants go to `<moodle-dir>/local/unenrol_expired/cli`
and enter `php unenrol_expired.php`.

To automatically unenroll expired participants create the file
`/etc/cron.d/unenrol_expired` with the content:

```
# /etc/cron.d/unenrol_expired
# unenroll moodle users from their course if enrolment time has ended

0 0 * * * root (cd /var/www/html/moodle/local/unenrol_expired/cli/ ; php unenrol_expired.php)

```

Change the path to the script if your moodle installation is in a
different directory.

In this example the unenrolment script is executed every day at midnight.
If you want the script to run at different times or intervals adapt the
crontab entry accordingly (see `man 5 crontab`).

## License ##

2021 Peter Parzer <peter.parzer@med.uni-heidelberg.de>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
